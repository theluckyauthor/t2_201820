package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ListIterator;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IteradorLista;
import model.data_structures.ListaDoblementeEncadenada;

public class DivvyTripsManager implements IDivvyTripsManager {

private ListaDoblementeEncadenada<VOTrip> trips;
private ListaDoblementeEncadenada<VOStation> stations;

	
	public void loadStations (String stationsFile) {
		stations = new ListaDoblementeEncadenada<VOStation>();
		try
		{
			FileReader fr =  new FileReader(stationsFile);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			String data = br.readLine();
			while(data != null)
			{
				String[] datos = data.split(",");
				int id = Integer.parseInt(datos[0]);
				String name = datos[1];
				String city =  datos[2];
				double latitude =  Double.parseDouble(datos[3]);
				double longitude =  Double.parseDouble(datos[4]);
				int dpcapacity =  Integer.parseInt(datos[5]);
				String online_date =  datos[6];
				data = br.readLine();
				stations.addAtBeginning(new VOStation(id, name, city, latitude, longitude, dpcapacity, online_date));
			}
			br.close();
			fr.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	
	public void loadTrips (String tripsFile) {
		trips = new ListaDoblementeEncadenada<VOTrip>();
		try
		{
			// quitar que en cada add se revise si el identificador es �nico
			FileReader fr =  new FileReader(tripsFile);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			String data = br.readLine();
			while(data != null)
			{
				String[] datos = data.split(",");
				int trip_id = Integer.parseInt(datos[0]);
				String start_time = datos[1];
				String end_time = datos[2];
				int bikeid = Integer.parseInt(datos[3]);
				int tripduration = Integer.parseInt(datos[4]);
				int from_station_id = Integer.parseInt(datos[5]);
				String from_station_name = datos[6];
				int to_station_id = Integer.parseInt(datos[7]);
				String to_station_name = datos[8];
				String usertype = datos[9];
				String gender;
				String birthyear;
				
				if(datos.length == 10)
				{
					gender = "";
					birthyear = "";
				}
				else if(datos.length == 11)
				{
					gender = datos[10];
					birthyear ="";
				}
				else
				{
					gender = datos[10];
					birthyear = datos[11];
				}
				data = br.readLine();
				trips.addAtBeginning(new VOTrip(trip_id, start_time, end_time, bikeid, tripduration, from_station_id, from_station_name, to_station_id, to_station_name, usertype, gender, birthyear));
			}
			br.close();
			fr.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
	}
	
	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		ListaDoblementeEncadenada<VOTrip> list = new ListaDoblementeEncadenada<>();
		ListIterator<VOTrip> i = trips.listIterator();
		while(i.hasNext())
		{
			VOTrip j = i.next();
			if(j.getGender().equalsIgnoreCase(gender))
			{
				list.addAtBeginning(j);
			}
		}
		return list;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		ListaDoblementeEncadenada<VOTrip> list = new ListaDoblementeEncadenada<>();
		ListIterator<VOTrip> i = trips.listIterator();
		while(i.hasNext())
		{
			VOTrip j = i.next();
			if(j.getToStationId()==stationID)
			{
				list.addAtBeginning(j);
			}
		}
		return list;
	}	


}
