package model.vo;

import model.data_structures.Identificador;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Identificador{

	public int id;
	public String start_time;
	public String end_time;
	public int bikeid;
	public int tripduration;
	public int from_station_id;
	public String from_station_name;
	public int to_station_id;
	public String to_station_name;
	public String usertype;
	public String gender;
	public String birthyear;
	
	public VOTrip(int trip_id, String pstart_time, String pend_time, int bikeid2,int tripduration2, int from_station_id2, String pfrom_station_name, int to_station_id2, String pto_station_name, String pusertype, String pgender, String pbirthyear )
	{
		id = trip_id;
		start_time = pstart_time;
		end_time = pend_time;
		bikeid = bikeid2;
		tripduration = tripduration2;
		from_station_id = from_station_id2;
		from_station_name = pfrom_station_name;
		to_station_id = to_station_id2;
		to_station_name = pto_station_name;
		usertype = pusertype;
		gender = pgender;
		birthyear = pbirthyear;
		
	}
	public int getid()
	{
		return id;
	}
	public String getEnd_time()
	{
		return end_time;
	}
	public String getStart_time()
	{
		return start_time;
	}
	public int getBikeid()
	{
		return bikeid;
	}
	public int getTripDuration()
	{
		return tripduration;
	}
	public int getFromStationId()
	{
		return from_station_id;
	}
	public String getFromStationName()
	{
		return from_station_name;
	}
	public int getToStationId()
	{
		return to_station_id;
	}
	public String getToStationName()
	{
		return to_station_name;
	}
	public String getUsertype()
	{
		return usertype;
	}
	public String getGender()
	{
		return gender;
	}
	public String getBirthyear()
	{
		return birthyear;
	}
	@Override
	public String darIdentificador() {
		// TODO Auto-generated method stub
		return String.valueOf(id);
	}
}
