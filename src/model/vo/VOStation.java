package model.vo;

import model.data_structures.Identificador;

/**
 * Representation of a station object
 */
public class VOStation implements Identificador {

private int id;
private String name;
private String city;
private double latitude;
private double longitude;
private int dpcapacity;
private String online_date;


public VOStation(int pid, String pname, String pcity, double platitude, double plongitude, int pdpcapacity, String ponline_date)
{
	id = pid;
	name = pname;
	city = pcity;
	latitude = platitude;
	longitude = plongitude;
	dpcapacity = pdpcapacity;
	online_date = ponline_date;
}
	@Override
	public String darIdentificador() {
		// TODO Auto-generated method stub
		return String.valueOf(id);
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getCity() {
		return city;
	}
	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public int getdpCapacity() {
		return dpcapacity;
	}
	public String getonline_date() {
		return online_date;
	}
}
