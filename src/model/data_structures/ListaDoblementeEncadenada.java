package model.data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Clase que representa la lista doblemente encadenada
 *  @author Christian Camilo Aparicio Baquen para el ejercicio de Nivel 9 de APO2
 * Modificado por: Daniel Felipe Serrano Mora
 * @param <E> Tipo de los objetos que almacenar� la lista.
 */
public class ListaDoblementeEncadenada<E extends Identificador> extends ListaEncadenadaAbstracta<E> implements DoublyLinkedList<E>
{

	/**
	 *  Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;

	//atributo
	public NodoListaDoble siguienteElemento;
	/**
	 * Construye una lista vacia
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public ListaDoblementeEncadenada() 
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaDoblementeEncadenada(E nPrimero)
	{
		if(primerNodo == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		primerNodo = new NodoListaDoble<E>(nPrimero);
		cantidadElementos = 1;
	}

	/**
	 * Agrega un elemento al final de la lista
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
	 * Se actualiza la cantidad de elementos.
	 * @param e el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean add(E e) 
	{
		if (e == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		else
		{
			NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
			if(actual == null)
			{
				actual =  new NodoListaDoble<E>(e);
				primerNodo = actual;
				cantidadElementos++;
				return true;

			}
			else
			{
				while (actual.darSiguiente()!= null)
				{
					//if(actual.darElemento().darIdentificador().equals(e.darIdentificador()))
					//{
					//	return false;
					//}
					actual = (NodoListaDoble<E>) actual.darSiguiente();
				}
				NodoListaDoble<E> nuevo = new NodoListaDoble<E>(e);
				actual.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(actual);
				cantidadElementos++;
				return true;
			}
		}
		// TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Agrega un elemento al comienzo de la lista. Actualiza la cantidad de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elemento el elemento que se desea agregar. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean addAtBeginning(E e)
	{
		if (e == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		else
		{
			NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
				primerNodo = new NodoListaDoble<E>(e);
				primerNodo.cambiarSiguiente(actual);
				cantidadElementos++;
				return true;
		}
	}
	/**
	 * Agrega un elemento al en cualquier parte de la lista Actualiza la cantidad de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elemento el elemento que se desea agregar. 
	 * @param index el indice en d�nde se va a afregar
	 * @throws NullPointerException si el elemento es nulo
	 */
	// Corregir este m�todo para que agregue objectos iguales pero con diferente id
	public void add(int index, E elemento) 
	{
		boolean encontrado = false;
		if (elemento == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		if(index < 0 || index > cantidadElementos)
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + cantidadElementos);
		}
		NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
		NodoListaDoble<E> nuevo = new NodoListaDoble<E>(elemento);
		if (index == 0)
		{
			nuevo.cambiarAnterior(actual.darAnterior());
			nuevo.cambiarSiguiente(actual);
			primerNodo = nuevo;
			actual.cambiarAnterior(nuevo);
			cantidadElementos++;
		}
		else
		{
			for(int i = 0; i < index && encontrado == false; i++)
			{
				//if(actual.darElemento().darIdentificador().equals(elemento.darIdentificador()))
				//{
				//	encontrado = true;
				//}
				actual = (NodoListaDoble<E>) actual.darSiguiente();
			}
			if (encontrado == false) {
				nuevo.cambiarAnterior(actual.darAnterior());
				nuevo.cambiarSiguiente(actual);
				primerNodo = nuevo;
				actual.cambiarAnterior(nuevo);
				cantidadElementos++;
			}
		}
	}
	
		// TODO DONE Completar seg�n la documentaci�n		

	/**
	 * M�todo que retorna el iterador de la lista.
	 * @return el iterador de la lista.
	 */
	public ListIterator<E> listIterator() 
	{
		return new IteradorLista<E>((NodoListaDoble<E>) primerNodo);
	}

	/**
	 * M�todo que retorna el iterador de la lista desde donde se indica.
	 * @param index �ndice desde se quiere comenzar a iterar.
	 * @return el iterador de la lista.
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public ListIterator<E> listIterator(int index) 
	{
		if(index< 0 || index >= size())
			throw new IndexOutOfBoundsException("El �ndice buscado est� por fuera de la lista.");
		return new IteradorLista<E>((NodoListaDoble<E>) darNodo(index));
	}

	/**
	 * Elimina el nodo que contiene al objeto que llega por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param objeto el objeto que se desea eliminar. objeto != null
	 * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
	 */
	public boolean remove(Object o) 
	{
		boolean pude = false;
		NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
		if(actual == null)
		{
			throw new NullPointerException("lista vac�a");
		}
		else if(actual.darElemento().equals(o))
		{
			primerNodo = (NodoListaDoble<E>) actual.darSiguiente();
			actual.cambiarElemento(null);
			cantidadElementos--;
			pude = true;
		}
		else
		{
			while(actual.darSiguiente().darSiguiente()!=null && pude== false)
			{
				if(actual.darSiguiente().darElemento().equals(o))
				{
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
					siguienteElemento =  (NodoListaDoble<E>) actual.darSiguiente();
					siguienteElemento.cambiarAnterior((NodoListaDoble<E>)actual);
					cantidadElementos--;
					pude = true;
				}
				actual = (NodoListaDoble<E>) actual.darSiguiente();
			}
			if (actual.darSiguiente().darSiguiente().darElemento().equals(o))
			{
				actual.darSiguiente().cambiarSiguiente(null);
				cantidadElementos--;
				pude = true;
			}
		}
		return pude;
		// TODO  DONE Completar seg�n la documentaci�n
	}
	
	public boolean deleteAtBeginning() 
	{
		boolean pude = false;
		NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
		if(actual == null)
		{
			throw new NullPointerException("lista vac�a");
		}
		else
		{
			primerNodo = (NodoListaDoble<E>) actual.darSiguiente();
			actual.cambiarElemento(null);
			cantidadElementos--;
			pude = true;
		}		
		return pude;
		// TODO  DONE Completar seg�n la documentaci�n
	}

	/**
	 * Elimina el nodo en la posici�n por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param pos la posici�n que se desea eliminar
	 * @return el elemento eliminado
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public E remove(int index) 
	{
		E e = null;
		if(index < 0 || index >= size())
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + cantidadElementos);
		}
		else
		{
			NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;

			if (index == 0)
			{
				e = actual.darElemento();
				primerNodo = actual.darSiguiente();
				actual.cambiarElemento(null);
				cantidadElementos--;
			}
			else
			{
				NodoListaDoble<E> anterior = null;
				for(int i = 0; i < index-1; i++)
				{
					anterior = actual;
					actual = (NodoListaDoble<E>) actual.darSiguiente();
				}
				e = actual.darElemento();
				anterior.cambiarSiguiente(actual.darSiguiente());
				actual = anterior;
			}
		}
		return e;
		// TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro.
	 * Actualiza la cantidad de elementos
	 * @param coleccion la colecci�n de elementos a mantener. coleccion != null
	 * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
	 */
	public boolean retainAll(Collection<?> c) 
	{
		boolean elimine = false;
		NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
		while(actual.darElemento() != null)
		{
			if(c.contains(actual.darElemento()) == false)
			{
				remove(actual.darElemento());
				elimine = true;
				cantidadElementos--;

			}
		}
		return elimine;
		// TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Crea una lista con los elementos de la lista entre las posiciones dadas
	 * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
	 * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
	 * @return una lista con los elementos entre las posiciones dadas
	 * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
	 */
	public List<E> subList(int inicio, int fin) 
	{
		List <E> lista = new ListaDoblementeEncadenada<E>();
		NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
		if(inicio < 0 || fin >= cantidadElementos || fin < inicio)
		{
			throw new IndexOutOfBoundsException("Valores de inicio y fin invalidos");
		}
		else
		{
			for(int i = 0; i< inicio; i++)
			{
				actual = (NodoListaDoble<E>) actual.darSiguiente();
			}
			for(int i = inicio; i <= fin; i++)
			{
				lista.add(actual.darElemento());
				actual = (NodoListaDoble<E>) actual.darSiguiente();	
			}

		}
		return lista;
		// TODO DONE Completar seg�n la documentaci�n
	}

	@Override
	public boolean addAtEnd(E e) {
		
		return add(e);
	}

	@Override
	public void addAtK(int index, E element) {
		add(index, element);
		
	}

	@Override
	public E getElement(int e) {
		return  get(e);
	}

	@Override
	public int getSize() {
		return size();
	}


	@Override
	public boolean delete(Object o) {
		return remove(o);
	}

	@Override
	public E deleteAtK(int index) {
		return remove(index);
	}



}
