package model.data_structures;

import java.util.Collection;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
/**
@author Christian Camilo Aparicio Baquen para el ejercicio de Nivel 9 de APO2
* Modificado por: Daniel Felipe Serrano Mora
* */
public class ListaSencillamenteEncadenada<E extends Identificador> extends ListaEncadenadaAbstracta<E>
{

	/**
	 * Constante de serializaci�n.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Construye la lista vac�a.
	 * <b>post: </b> Se ha inicializado el primer nodo en null
	 */
	public ListaSencillamenteEncadenada() 
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�metro. Actualiza el n�mero de elementos.
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaSencillamenteEncadenada(E nPrimero)
	{
		if(nPrimero == null)
			throw new NullPointerException("Se recibe un elemento nulo");
		this.primerNodo = new NodoListaSencilla<E>(nPrimero);
		cantidadElementos = 1;
	}

	/**
	 * Agrega un elemento al final de la lista, actualiza el n�mero de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elem el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean add(E elemento) throws NullPointerException 
	{
		if (elemento == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		else
		{
			NodoListaSencilla<E> actual = primerNodo;
			if(actual == null)
			{
				actual =  new NodoListaSencilla<E>(elemento);
				primerNodo = actual;
				cantidadElementos++;
				return true;
			}
			else
			{
				while (actual.darSiguiente()!= null)
				{
					if(actual.darElemento().darIdentificador().equals(elemento.darIdentificador()))
					{
						return false;
					}
					actual = actual.darSiguiente();
				}
				actual.cambiarSiguiente(new NodoListaSencilla<E>(elemento));
				cantidadElementos++;
				return true;
			}

		}
		// TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Agrega un elemento en la posici�n dada de la lista. Todos los elementos siguientes se desplazan.
	 * Actualiza la cantidad de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param pos la posici�n donde se desea agregar. Si pos es igual al tama�oo de la lista se agrega al final
	 * @param elem el elemento que se desea agregar
	 * @throws IndexOutOfBoundsException si el inidice es < 0 o > size()
	 * @throws NullPointerException Si el elemento que se quiere agregar es null.
	 */
	public void add(int index, E elemento) 
	{
		boolean encontrado = false;
		if (elemento == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		if(index < 0 || index > cantidadElementos)
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + cantidadElementos);
		}
		NodoListaSencilla<E> actual = primerNodo;
		NodoListaSencilla<E> nuevo = new NodoListaSencilla<E>(elemento);
		if (index == 0 && contains(elemento))
		{
			nuevo.cambiarSiguiente(actual);
			primerNodo = nuevo;
			cantidadElementos++;
		}
		else
		{
			for(int i = 0; i < index && encontrado == false; i++)
			{
				if(actual.darElemento().darIdentificador().equals(elemento.darIdentificador()))
				{
					encontrado = true;
				}
				actual = actual.darSiguiente();

			}
			if (encontrado == false)
			{
				nuevo.cambiarSiguiente(actual.darSiguiente());
				actual.cambiarSiguiente(nuevo);
				cantidadElementos++;	
			}

		}
		// TODO DONE Completar seg�n la documentaci�n	
	}

	@Deprecated
	public ListIterator<E> listIterator() 
	{
		throw new UnsupportedOperationException ();
	}

	@Deprecated
	public ListIterator<E> listIterator(int index) 
	{
		throw new UnsupportedOperationException ();
	}


	/**
	 * Elimina el nodo que contiene al objeto que llega por par�metro. Actualiza el n�mero de elementos.
	 * @param objeto el objeto que se desea eliminar. objeto != null
	 * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
	 */
	public boolean remove(Object objeto) 
	{
		boolean pude = false;
		NodoListaSencilla<E> actual = primerNodo;
		if(actual.darElemento().equals(objeto))
		{
			primerNodo = actual.darSiguiente();
			cantidadElementos--;
			pude = true;
		}
		else
		{
			while(actual.darSiguiente().darSiguiente()!=null)
			{
				if(actual.darSiguiente().darElemento().equals(objeto))
				{
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
					cantidadElementos--;
					pude = true;
				}
				actual = actual.darSiguiente();
			}
			if (actual.darSiguiente().darSiguiente().darElemento().equals(objeto))
			{
				actual.darSiguiente().cambiarSiguiente(null);
				cantidadElementos--;
				pude = true;
			}
		}
		return pude;
		//TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Elimina el nodo en la posici�n por par�metro. Actualiza la cantidad de elementos.
	 * @param pos la posici�n que se desea eliminar
	 * @return el elemento eliminado
	 * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
	 */
	public E remove(int pos) 
	{
		E e = null;
		if(pos < 0 || pos >= size())
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + pos + " y el tama�o de la lista es de " + cantidadElementos);
		}
		else
		{
			NodoListaSencilla<E> actual = primerNodo;

			if (pos == 0)
			{
				e = actual.darElemento();
				primerNodo = actual.darSiguiente();
			}
			else
			{
				NodoListaSencilla<E> anterior = null;
				for(int i = 0; i < pos-1; i++)
				{
					anterior = actual;
					actual = actual.darSiguiente();
				}
				e = actual.darElemento();
				anterior.cambiarSiguiente(actual.darSiguiente());
			}
		}
		return e;
		//TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro. La cantidad de elementos se actualiza.
	 * @param coleccion la colecci�n de elementos a mantener. coleccion != null
	 * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
	 */
	public boolean retainAll(Collection<?> coleccion) 
	{
		boolean elimine = false;
		NodoListaSencilla<E> actual = primerNodo;
		while(actual.darElemento() != null)
		{
			if(coleccion.contains(actual.darElemento()) == false)
			{
				remove(actual.darElemento());
				elimine = true;
				cantidadElementos--;

			}
		}
		return elimine;
		//TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Crea una lista con los elementos de la lista entre las posiciones dadas
	 * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
	 * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
	 * @return una lista con los elementos entre las posiciones dadas
	 * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
	 */
	public List<E> subList(int inicio, int fin) 
	{
		List <E> lista = new ListaSencillamenteEncadenada<E>();
		NodoListaSencilla<E> actual = primerNodo;
		if(inicio < 0 || fin >= cantidadElementos || fin < inicio)
		{
			throw new IndexOutOfBoundsException("Valores de inicio y fin invalidos");
		}
		else
		{
			for(int i = 0; i< inicio; i++)
			{
				actual = actual.darSiguiente();
			}
			for(int i = inicio; i <= fin; i++)
			{
				lista.add(actual.darElemento());
				actual = actual.darSiguiente();	

			}

		}
		return lista;
		//TODO DONE Completar seg�n la documentaci�n
	}

}
