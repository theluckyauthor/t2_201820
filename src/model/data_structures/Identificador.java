package model.data_structures;

import java.io.Serializable;

/**
 * Contrato que deben cumplir los elementos identificados de forma �nica.
 * @author Christian Camilo Aparicio Baquen para el ejercicio de Nivel 9 de APO2
 * Modificado por: Daniel Felipe Serrano Mora
 */
public interface Identificador
{
	/**
	 * M�todo que retorna el identificador �nico del elemento.
	 * @return
	 */
	public String darIdentificador();
	

}
