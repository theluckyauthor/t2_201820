package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * addAtBeginning, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * 
 *  probbarlos con el nodo: next, previous, getCurrentElement
 * @param <E>
 */
public interface DoublyLinkedList<E> extends Iterable<E> {

	boolean addAtBeginning(E e);
	boolean addAtEnd(E e);
	void addAtK(int index, E element);
	E getElement(int e);
	int getSize();
	boolean deleteAtBeginning();
	boolean delete(Object o);
	E deleteAtK(int index);	
}
