package model.data_structures;

import java.io.Serializable;
import java.util.ListIterator;

/**
 * Clase que representa el iterador de lista (avanza hacia adelante y hacia atr�s)
 *  @author Christian Camilo Aparicio Baquen para el ejercicio de Nivel 9 de APO2
 * Modificado por: Daniel Felipe Serrano Mora
 * @param <E> Tipo del objeto que almacena el iterador de la lista
 */
public class IteradorLista<E extends Identificador> implements ListIterator<E>
{
	/**
	 * Nodo anterior al que se encuentra el iterador.
	 */
	private NodoListaDoble<E> anterior;

	/**
	 * Nodo en el que se encuentra el iterador.
	 */
	private NodoListaDoble<E> actual;

	/**
	 * Crea un nuevo iterador de lista iniciando en el nodo que llega por par�metro
	 * @param primerNodo el nodo en el que inicia el iterador. nActual != null
	 */
	public IteradorLista(NodoListaDoble<E> primerNodo)
	{
		actual = primerNodo;
		anterior = null;
	}

	/**
	 * Indica si hay nodo siguiente
	 * true en caso que haya nodo siguiente o false en caso contrario
	 */
	public boolean hasNext() 
	{
		return actual.darSiguiente()!= null;
		// TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Indica si hay nodo anterior
	 * true en caso que haya nodo anterior o false en caso contrario
	 */
	public boolean hasPrevious() 
	{
		return actual.darAnterior()!= null;
		// TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Devuelve el elemento siguiente de la iteraci�n y avanza.
	 * @return elemento siguiente de la iteraci�n
	 */
	public E next() 
	{
		actual = (NodoListaDoble<E>) actual.darSiguiente();
		return  actual.darElemento();
		// TODO DONE Completar seg�n la documentaci�n
	}

	/**
	 * Devuelve el elemento anterior de la iteraci�n y retrocede.
	 * @return elemento anterior de la iteraci�n.
	 */
	public E previous() 
	{
		actual = actual.darAnterior();
		return  actual.darElemento();
		// TODO DONE Completar seg�n la documentaci�n
	}


	//=======================================================
	// M�todos que no se implementar�n
	//=======================================================

	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(E e) 
	{
		throw new UnsupportedOperationException();
	}

	public void add(E e) 
	{
		throw new UnsupportedOperationException();		
	}

}
