package model.data_structures;

import java.io.Serializable;
/**
 * @author Christian Camilo Aparicio Baquen para el ejercicio de Nivel 9 de APO2
 * Modificado por: Daniel Felipe Serrano Mora
 * @author df.serrano
 *
 * @param <E> el elemento que se guerda en el nodo
 */
public class NodoListaSencilla<E extends Identificador> implements Serializable
{

	/**
	 * Constante de Serialización
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Elemento almacenado en el nodo.
	 */
	protected E elemento;

	/**
	 * Siguiente nodo.
	 */
	//TODO DONE Defina el atributo siguiente como el siguiente nodo de la lista.
	private NodoListaSencilla siguiente;

	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenará en el nodo. elemento != null
	 */
	public NodoListaSencilla(E elemento)
	{
		this.elemento = elemento;
		//TODO DONE Completar de acuerdo a la documentación.
	}

	/**
	 * Método que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(NodoListaSencilla<E> siguiente)
	{
		this.siguiente = siguiente;
		//TODO DONE Completar de acuerdo a la documentación.
	}

	/**
	 * Método que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public E darElemento()
	{
		return elemento;
		//TODO DONE Completar de acuerdo a la documentación.
	}

	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenará en el nodo.
	 */
	public void cambiarElemento(E elemento)
	{
		this.elemento = elemento;
		//TODO DONE Completar de acuerdo a la documentación.
	}

	/**
	 * Método que retorna el identificador del nodo.
	 * Este identificador es el identificador del elemento que almacena.
	 * @return Identificador del nodo (identificador del elemento almacenado).
	 */
	public String darIdentificador()
	{
		return this.elemento.darIdentificador();
		//TODO DONECompletar de acuerdo a la documentación.
	}

	/**
	 * Método que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public NodoListaSencilla<E> darSiguiente()
	{
		return this.siguiente;
		//TODO DONE Completar de acuerdo a la documentación.
	}

}
