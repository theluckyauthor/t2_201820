package model.data_structures;
/**
 * 
 * @author df.serrano
 *@author Christian Camilo Aparicio Baquen para el ejercicio de Nivel 9 de APO2
 * Modificado por: Daniel Felipe Serrano Mora
 * @param <E> el elemento que se almacenaré en la lista
 */
public class NodoListaDoble<E extends Identificador> extends NodoListaSencilla<E> implements DoublyLinkedNode<E>
{
	/**
	 * Constante de serialización
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nodo anterior.
	 */
	private NodoListaDoble anterior;
	//TODO DONE Defina el atributo anterior como el nodo anterior de la lista.

	/**
	 * Método constructor del nodo doblemente encadenado
	 * @param elemento elemento que se almacenará en el nodo.
	 */
	public NodoListaDoble(E elemento) 
	{
		super(elemento);
		//TODO DONE Completar de acuerdo a la documentación.
	}

	/**
	 * Método que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public NodoListaDoble<E> darAnterior()
	{
		return anterior;
		//TODO DONE Completar de acuerdo a la documentación.
	}

	/**
	 * Método que cambia el nodo anterior por el que llega como parámetro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarAnterior(NodoListaDoble<E> anterior)
	{
		this.anterior = anterior;
		//TODO DONE Completar de acuerdo a la documentación.
	}

	@Override
	public E next() {
		
		return darSiguiente().darElemento();
	}

	@Override
	public E previous() {
		
		return darAnterior().darElemento();
	}

}
