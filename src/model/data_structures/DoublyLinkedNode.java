package model.data_structures;

public interface DoublyLinkedNode<E> {

	 E next();
	 E previous();
}
