package model.data_structures;

import java.util.Comparator;

/**
 * Estructura de datos lista ordenada por alg�n criterio de ordenamiento de E
 * @author Christian Camilo Aparicio Baquen para el ejercicio de Nivel 9 de APO2
 * Modificado por: Daniel Felipe Serrano Mora
 *
 * @param <E> Tipo de dato que se almacenar� en la lista, debe ser Unicamente identificado
 */
public class ListaSencillamenteEncadenadaOrdenada<E extends Identificador> extends ListaSencillamenteEncadenada<E>
{
	/**
	 * Comparador de E, permite comparar dos E para mantener el orden en la lista.
	 */
	private Comparator<E> comparador;

	/**
	 * Indica si la lista debe organizarse ascendente o descendentemente
	 */
	private boolean ascendente;

	/**
	 * Construye una lista vac�a.
	 * <b>post: </b> - Se ha inicializado el primer nodo en null. <br/>
	 * - Se ha inicializado el criterio de comparaci�n por el que se ordenar� el elemento.
	 * - Se ha inicializado si se quiere ordenar ascendente.
	 * @param comparador Criterio de comparaci�n por el que se ordenar�n los elementos en la lista.
	 */
	public ListaSencillamenteEncadenadaOrdenada(Comparator<E> comparador, boolean ascendente)
	{
		this.comparador = comparador;
		this.ascendente = ascendente;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�metro. Actualiza el n�mero de elementos.
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @param comparador Criterio de comparaci�n por el que se ordenar�n los elementos en la lista.
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaSencillamenteEncadenadaOrdenada(E nPrimero, Comparator<E> comparador, boolean ascendente)
	{
		super(nPrimero);
		this.comparador = comparador;
		this.ascendente = ascendente;
	}


	/**
	 * Agrega un elemento a la lista manteniendo el orden de acuerdo al criterio de comparaci�n, actualiza el n�mero de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
	 * @param elem el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	@Override
	public boolean add(E elemento) 
	{
		boolean pude = false;
		NodoListaSencilla<E> anterior = null;
		NodoListaSencilla<E> actual = primerNodo;
		NodoListaSencilla<E> nuevo = new NodoListaSencilla<E>(elemento);
		if(elemento == null)
		{
			throw new NullPointerException("Elemento Nulo");
		}
		else if(ascendente == true && actual != null)
		{
			while(actual.darSiguiente() != null && comparador.compare(elemento, actual.darElemento())== 1)
			{
				anterior = actual;
				actual = actual.darSiguiente();
			}
			if(actual.darElemento().darIdentificador().equals(elemento.darIdentificador()))
			{
				pude = false;
			}
			else if (actual.darSiguiente() == null)
			{
				actual.cambiarSiguiente(nuevo);
				cantidadElementos++;
				pude = true;
			}
			else if (anterior != null)
			{
				nuevo.cambiarSiguiente(actual);
				anterior.cambiarSiguiente(nuevo);
				cantidadElementos++;
				pude = true;
			}
			else
			{
				nuevo.cambiarSiguiente(actual);
				primerNodo = nuevo;
				cantidadElementos++;
				pude = true;
			}
		}
		else if(ascendente == false && actual != null)
		{
			while(actual.darSiguiente() != null && comparador.compare(elemento, actual.darElemento())== -1)
			{
				anterior = actual;
				actual = actual.darSiguiente();
			}
			if(actual.darElemento().darIdentificador().equals(elemento.darIdentificador()))
			{
				pude = false;
			}
			else if (actual.darSiguiente() == null)
			{
				actual.cambiarSiguiente(nuevo);
				cantidadElementos++;
				pude = true;
			}
			else if (anterior != null)
			{
				nuevo.cambiarSiguiente(actual);
				anterior.cambiarSiguiente(nuevo);
				cantidadElementos++;
				pude = true;
			}
			else
			{
				nuevo.cambiarSiguiente(actual);
				primerNodo = nuevo;
				cantidadElementos++;
				pude = true;
			}
		}
		else
		{
			primerNodo = new NodoListaSencilla<E>(elemento);
			pude = true;
			cantidadElementos++;
		}
		return pude;
		//TODO DONE Completar seg�n la documentaci�n.
	}




	//------------------------------------------------------------------------------------------
	// M�todos no soportados por la lista (No los soporta porque no tienen sentido, la lista siempre debe estar organizada por el criterio de comparaci�n).
	//-----------------------------------------------------------------------------------------
	@Override
	@Deprecated
	public void add(int index, E elemento) 
	{
		throw new UnsupportedOperationException("No se puede hacer uso de esta operaci�n en este tipo de lista");
	}

	@Override
	@Deprecated
	public E set(int index, E element) throws IndexOutOfBoundsException 
	{
		throw new UnsupportedOperationException("No se puede hacer uso de esta operaci�n en este tipo de lista");
	}

}
