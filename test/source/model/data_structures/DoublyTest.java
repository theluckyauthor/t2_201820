package model.data_structures;

	import org.junit.Test;
	import static org.junit.Assert.*;
	import org.junit.Before;
	import model.data_structures.*;
	import model.logic.*;
	import model.vo.*;
public class DoublyTest {
		public ListaDoblementeEncadenada<VOTrip> tripsList;
		@Before
		public void setUpEscenario1(){
			tripsList = new ListaDoblementeEncadenada<VOTrip>();
			tripsList.add(new VOTrip(100,"a","b",200, 10, 300, "t", 400, "r", "ee", "f", "g"));
			tripsList.add(new VOTrip(200,"e","c",600, 20, 700, "s", 800, "o", "ee", "m", "gg"));
			tripsList.add(new VOTrip(300,"i","d",400, 30, 600, "c", 800, "o", "ee", "f", "g"));
			tripsList.add(new VOTrip(400,"o","b",300, 40, 500, "q", 700, "s", "ee", "m", "gg"));
			tripsList.add(new VOTrip(500,"u","c",800, 50, 700, "s", 600, "c", "e", "f", "g"));
			tripsList.add(new VOTrip(600,"a","d",400, 60, 300, "t", 200, "d", "e", "m", "gg"));
			tripsList.add(new VOTrip(700,"e","b",200, 70, 600, "c", 500, "q", "e", "f", "gg"));
			tripsList.add(new VOTrip(800,"i","c",900, 80, 900, "n", 100, "u", "e", "m", "gg"));
			
		}
		@Test
		public void testAddAtBeginning()
		{
			setUpEscenario1();
			tripsList.addAtBeginning(new VOTrip(900,"i","c",900, 80, 900, "n", 100, "u", "e", "m", "gg"));
			assertEquals("No agrega el elemento al comienzo", 900, tripsList.darNodo(0).darElemento().getid());
		}
		@Test
		public void testAddAtEnd()
		{
			setUpEscenario1();
			tripsList.add(new VOTrip(900,"i","c",900, 80, 900, "n", 100, "u", "e", "m", "gg"));
			assertEquals("No agrega el elemento al final", 900, tripsList.darNodo(8).darElemento().getid());
			
		}
		@Test
		public void testAddAtK()
		{
			setUpEscenario1();
			tripsList.addAtK(0,new VOTrip(900,"i","c",900, 80, 900, "n", 100, "u", "e", "m", "gg"));
			assertEquals("No agrega el elemento al �ndice indicado", 900, tripsList.darNodo(0).darElemento().getid());
		}
		@Test
		public void testGetElement()
		{
			setUpEscenario1();
			assertEquals("No da el elemento correcto", 100, tripsList.getElement(0).getid());
		}
		@Test
		public void testGetSize()
		{
			setUpEscenario1();
			assertEquals("No da el tama�o correcto", 8, tripsList.getSize());
		}
		@Test
		public void testDeleteAtBeginning()
		{
			setUpEscenario1();
			tripsList.deleteAtBeginning();
			assertNotEquals("No elimina el elemento correctamente", 100, tripsList.get(0).getid());
		}
		@Test
		public void testDelete()
		{
			setUpEscenario1();
			VOTrip e = new VOTrip(900,"i","c",900, 80, 900, "n", 100, "u", "e", "m", "gg");
			tripsList.addAtBeginning(e);
			tripsList.delete(e);
			assertNotEquals("No elimina el elemento correctamente", 900, tripsList.get(0).getid());
		}
		@Test
		public void testNext()
		{
			setUpEscenario1();
			assertEquals("No da el siguiente elemento correctamente", 200, tripsList.darNodo(0).darSiguiente().darElemento().getid());
		}
		@Test
		public void testPrevious()
		{
			setUpEscenario1();
			assertEquals("No da el anterior elemento correctamente", 100, ((NodoListaDoble<VOTrip>)tripsList.darNodo(1)).darAnterior().darElemento().getid());
		}
	}

